
/**
 * ConversorCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.1  Built on : Aug 31, 2011 (12:22:40 CEST)
 */

    package webservices;

    /**
     *  ConversorCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ConversorCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ConversorCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ConversorCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for milhaParaQuilometro method
            * override this method for handling normal response from milhaParaQuilometro operation
            */
           public void receiveResultmilhaParaQuilometro(
                    webservices.ConversorStub.MilhaParaQuilometroResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from milhaParaQuilometro operation
           */
            public void receiveErrormilhaParaQuilometro(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for quilometroParaMilha method
            * override this method for handling normal response from quilometroParaMilha operation
            */
           public void receiveResultquilometroParaMilha(
                    webservices.ConversorStub.QuilometroParaMilhaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from quilometroParaMilha operation
           */
            public void receiveErrorquilometroParaMilha(java.lang.Exception e) {
            }
                


    }
    