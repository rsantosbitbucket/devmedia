package webservices;

public class Conversor {

	private static final double UMA_MILHA_EM_KM = 1.609344;

	public double milhaParaQuilometro(double milha) {
		return (milha * UMA_MILHA_EM_KM);
	}

	public double quilometroParaMilha(double quilometro) {
		return (quilometro / UMA_MILHA_EM_KM);
	}

}