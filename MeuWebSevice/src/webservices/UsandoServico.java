package webservices;

import org.apache.axis2.AxisFault;


import webservices.ConversorStub.MilhaParaQuilometro;
import webservices.ConversorStub.MilhaParaQuilometroResponse;
import webservices.ConversorStub.QuilometroParaMilha;
import webservices.ConversorStub.QuilometroParaMilhaResponse;

import java.rmi.RemoteException;

import javax.swing.JOptionPane;

public class UsandoServico {

	public static void main(String[] args) {

		Object[] opcoesDialogo = { "Milha -> Quilômetro", "Quilômetro -> Milha" };

		int escolha = JOptionPane.showOptionDialog(null, "Escolha a conversão desejada", "Conversor",
				JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, opcoesDialogo, opcoesDialogo[0]);

		double valor = Double.valueOf(JOptionPane.showInputDialog("Informe o valor:"));

		try {
			ConversorStub stub = new ConversorStub();
			switch (escolha) {
			case 0:
				MilhaParaQuilometro milha = new MilhaParaQuilometro();
				milha.setMilha(valor);
				MilhaParaQuilometroResponse respostaMilha = stub.milhaParaQuilometro(milha);
				JOptionPane.showMessageDialog(null, valor + " milhas = " + Math.round(respostaMilha.get_return()) + " quilômetros");
				break;
			case 1:
				QuilometroParaMilha km = new QuilometroParaMilha();
				km.setQuilometro(valor);
				QuilometroParaMilhaResponse respostaKm = stub.quilometroParaMilha(km);
				JOptionPane.showMessageDialog(null, valor + " quilômetros = " + Math.round(respostaKm.get_return()) + " milhas");
				break;
			}
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
}
